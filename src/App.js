import React from 'react';
import LogIn from './components/containers/LogIn/LogIn';
import AppHeader from './components/shared/AppHeader/AppHeader'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import './App.css';
import TranslationPage from './components/containers/TranslationPage/TranslationPage';
import ProfilePage from './components/containers/ProfilePage/ProfilePage';


function App() {

  

  return (
    <Router>
      <div className="App">
        <AppHeader></AppHeader>

        <main>
          <Switch>
            <Route exact path="/" component={LogIn}></Route>
            <Route path="/translate" component={TranslationPage}></Route>
            <Route path="/profile" component={ProfilePage}></Route>
          </Switch>
        </main>
      </div>
    </Router>
  );
}

export default App;
