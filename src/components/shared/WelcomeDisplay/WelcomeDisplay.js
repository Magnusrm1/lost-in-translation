import React from 'react';
import logo from '../../../assets/images/Logo-Hello.png';
import './WelcomeDisplay.css';

function WelcomeDisplay() {
    return (
        <div className="WelcomDisplayContainer">
            <div className="WelcomeContent">
                <img className="WelcomeLogo" src={logo} alt="Logo"></img>
                <div className="WelcomeTextContainer">
                    <h1>Lost in Translation</h1>
                    <h3>Get started</h3>
                </div>
            </div>
        </div>
    )
}
export default WelcomeDisplay;