
import React from 'react';
import './TranslationDisplay.css';

const TranslationDisplay = (props) => {

    const inputTextArray = props.inputTextArray;

    return (

        <div className="TranslationDisplayContainer">
            <ul className="Signs">
                {inputTextArray.map((value, index) => (
                    value.match(/[a-z|A-Z]/i) ? (
                        <li className="SignItem" key={index}>
                            <img className="SignImage" src={'/signs/' + value + '.png'} alt={"hand sign for " + value}></img>
                        </li>
                    ) : (
                            <li className="CharacterItem" key={index}>
                                <p className="CharacterDisplay" >{value}</p>
                            </li>
                        )
                ))}
            </ul>
            <div className="BottomSpacer"></div>
        </div>

    );
};
export default TranslationDisplay;