import React, { useEffect, useState } from 'react';
import userIcon from '../../../assets/images/user.png';
import { useHistory } from 'react-router-dom';
import './AppHeader.css'

function AppHeader() {

    const [user, setUser] = useState(null);
    const history = useHistory();

    useEffect(() => {
        const userString = window.localStorage.getItem('user');

        if (userString != null) {
            setUser(JSON.parse(userString));
        } else {
            setUser(null);
        }
    
    }, [history])

    const handleProfileClick = () => {
        history.push('/profile');
    }

    return (
        <header className="AppHeader">
            <div className="HeaderContent">
                <h3>Lost in Translation</h3>
            </div>
            {
                user ? (
                    <div className="ProfileContainer" onClick={handleProfileClick}>
                        <div className="ProfileName">
                            <p className="UserNameText">{user.username}</p>
                        </div>
                        <img className="UserIcon" src={userIcon} alt="User icon"></img>
                    </div>

                ) : (
                        <div></div>
                    )
            }

        </header>
    )
}
export default AppHeader;