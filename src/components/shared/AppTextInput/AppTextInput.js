import React from 'react';
import './AppTextInput.css';
import keyboard from '../../../assets/images/keyboard.png';
import rightArrow from '../../../assets/images/right-arrow.png';

const AppTextInput = (props) => {

    return (
        <div className="AppTextInputContainer" id={props.id}>
            <img className="KeyboardIcon" src={keyboard} alt="Keyboard icon"></img>
            <input type="text" onChange={props.change} className="InputField" placeholder={props.placeholderText}></input>
            <img className="SubmitButton" src={rightArrow} onClick={props.click} alt="arrow right, submit button"></img>
        </div>
    )
}
export default AppTextInput;