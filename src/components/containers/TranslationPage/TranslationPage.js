
import React, { useState } from 'react';
import AppTextInput from '../../shared/AppTextInput/AppTextInput';
import TranslationDisplay from '../../shared/TranslationDisplay/TranslationDisplay';
import './TranslationPage.css';

const TranslationPage = () => {

    const initialList = [];
    const [inputText, setInputText] = useState('');
    const [inputTextArray, setInputTextArray] = useState(initialList);



    const handleSubmitClick = () => {
        const user = window.localStorage.getItem('user');
        console.log('ok')
        if (user != null) {
            const userObject = JSON.parse(user);
            if (userObject.translations.length > 9) {
                userObject.translations.pop()
            }
            userObject.translations.push(inputText)
            window.localStorage.setItem('user', JSON.stringify(userObject));
        }
        const newList = inputText.split('');
        setInputTextArray(newList);
    };

    const handleInputChange = (event) => {
        setInputText(event.target.value);
        //setInputTextArray(event.target.value.split(''));
    };

    return (
        <div>
            <div className="TranslationPageContainer">
                <AppTextInput
                    id="traslationInput"
                    placeholderText="Translate something"
                    change={handleInputChange}
                    click={handleSubmitClick}
                ></AppTextInput>

            </div>
            <div>
                <TranslationDisplay inputTextArray={inputTextArray}></TranslationDisplay>
            </div>
        </div>

    );
};
export default TranslationPage;