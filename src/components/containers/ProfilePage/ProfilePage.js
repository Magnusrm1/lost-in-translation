
import React from 'react';
import TranslationDisplay from '../../shared/TranslationDisplay/TranslationDisplay';
import {useHistory} from 'react-router-dom';
import './ProfilePage.css';

const ProfilePage = () => {

    const history = useHistory();

    const user = JSON.parse(window.localStorage.getItem('user'));

    const handleLogOutClick = () =>{
        window.localStorage.clear();
        history.push('/');
    }

    const handleClearClick = () => {
        const user = JSON.parse(window.localStorage.getItem('user'));
        user.translations = [];
        window.localStorage.setItem('user', JSON.stringify(user));
    }

    return (
        <div>
            <div className="ProfileActionsContainer">
                <button className="LogOutButton" onClick={handleLogOutClick}>Log out</button>
                <button className="ClearTranslationsButton" onClick={handleClearClick}>Clear history</button>
            </div>
            <ul className="TranslationHistory">
                {
                    user.translations.map((value, index) => (
                        <li key={index} className="TranslationItem">
                            <TranslationDisplay inputTextArray={value.split('')}></TranslationDisplay>
                        </li>
                    ))
                }
                
            </ul>
        </div>

    );
};
export default ProfilePage;