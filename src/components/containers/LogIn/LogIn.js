import React, { useState } from 'react';
import { useHistory } from "react-router-dom";
import AppTextInput from '../../shared/AppTextInput/AppTextInput';
import WelcomeDisplay from '../../shared/WelcomeDisplay/WelcomeDisplay';
import './LogIn.css';

const LogIn = () => {
    const history = useHistory();

    const [username, setUsername] = useState('');

    const handleSubmitClick = () => {
        const user = {
            username: username,
            translations: []
        };
        window.localStorage.setItem('user', JSON.stringify(user));
        console.log("username: " + username);
        history.push('/translate');
    };

    const handleInputChange = (event) => {
        setUsername(event.target.value);
    };

    return (
        <div className="LogInContainer">
            <WelcomeDisplay></WelcomeDisplay>
            <div className="InputContainer">
                <div className="InputWrapper">
                    <AppTextInput
                        id="logInInput"
                        placeholderText="What is your name?"
                        change={handleInputChange}
                        click={handleSubmitClick}
                    ></AppTextInput>
                    <div className="BottomSpacer"></div>
                </div>
            </div>


        </div>
    );
};
export default LogIn;